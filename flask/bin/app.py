try:
    from PIL import Image, ImageDraw, ImageFont
except ImportError:
    import Image

from flask import Flask, jsonify, request, abort, make_response, Response
import io
import json
import tesspy as tess

APP = Flask(__name__)

@APP.route('/')
def index():
    return "Hello, World!"

#@app.route('/imageRecognition/api/v0.1/receive', methods=['GET'])
#def sendImageData():

@APP.route('/imageRecognition/api/v0.1/verify', methods=['POST'])
def recognize_image():
    if not request.files or not 'imagefile' in request.files:
        abort(400)
    image_file = request.files['imagefile']
    info_read = tess.image_ocr(image_file.read())
    
    return jsonify(info_read), 201

if __name__ == '__main__':
    APP.run(host='0.0.0.0', debug=True)

@APP.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)
