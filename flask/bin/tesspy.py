try:
    from PIL import Image, ImageDraw, ImageFont
except ImportError:
    import Image

import re
from matplotlib import pyplot as plt
import pytesseract as tess
import numpy as np
import cv2 as cv
import imutils as im

def count(COUNT):
    for c in COUNT:
        perl = cv.arcLength(c, True)
        approx = cv.approxPolyDP(c, 0.02 * perl, True)
        if len(approx) == 4:
            screenCnt = approx       
            return screenCnt   

def order_points(pts):
    rect = np.zeros((4,2), dtype = "float32")

    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect

def four_point_transform(image, pts):
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
 
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array([[0, 0], [maxWidth - 1, 0], [maxWidth - 1, maxHeight - 1], [0, maxHeight - 1]], dtype="float32")
 
    M = cv.getPerspectiveTransform(rect, dst)
    warped = cv.warpPerspective(image, M, (maxWidth, maxHeight))
 
    return warped

def image_ocr(image_file):
    
    image = np.asarray(bytearray(image_file), dtype="uint8")
    image = cv.imdecode(image, cv.IMREAD_COLOR)

    formatted_image = format_image(image)

    captured_info = read_image(formatted_image)

    return captured_info

def format_image(image_file):
    origin = image_file.copy()

    formatted_image = cv.cvtColor(image_file, cv.COLOR_BGR2GRAY)

    ret, formatted_image = cv.threshold(formatted_image, 2, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)

    formatted_image = cv.GaussianBlur(formatted_image, (5,5), 0)

    formatted_image = cv.adaptiveThreshold(formatted_image, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, \
    cv.THRESH_BINARY, 11, 2)

    formatted_image = cv.fastNlMeansDenoising(formatted_image, None, 100, 7, 23)
  
    formatted_image = cv.Canny(formatted_image, 75, 200)

    image_countours = cv.findContours(formatted_image.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
    image_countours = im.grab_contours(image_countours)
    image_countours = sorted(image_countours, key=cv.contourArea, reverse=True)[:5]

    screen_cnt = count(image_countours)
    reformatted_image = four_point_transform(origin, screen_cnt.reshape(4, 2))

    reformatted_image = cv.cvtColor(reformatted_image, cv.COLOR_BGR2GRAY)

    ret, reformatted_image = cv.threshold(reformatted_image, 2, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)

    reformatted_image = cv.GaussianBlur(reformatted_image, (5,5), 0)

    reformatted_image = cv.adaptiveThreshold(reformatted_image, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, \
    cv.THRESH_BINARY, 11, 2)

    reformatted_image = cv.fastNlMeansDenoising(reformatted_image, None, 100, 7, 23)

    return reformatted_image

def read_image(formatted_image):
    height, width = formatted_image.shape[:2]
    print(formatted_image.shape)
    height = int(height)
    width = int(width)
    print(height*1.0/width)

    if(height*1.0/width > 2):
        start_row, start_col = int(height*0.09), int(0)
        end_row, end_col = int(height*0.18), int(width*0.46)
        nome_endereco = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height*0.12), int(width*0.83)
        end_row, end_col = int(height*0.15), int(width*1)
        classe_instalacao = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height*0.08), int(width*0.72)
        end_row, end_col = int(height*0.12), int(width*1)
        num_cliente = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height*0.11), int(width*0.45)
        end_row, end_col = int(height*0.15), int(width*0.63)
        num_instalacao = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height * 0.615), int(width*0.3)
        end_row, end_col = int(height*0.75), int(width*0.45)
        his_consumo_dias = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height * 0.615), int(width*0.2)
        end_row, end_col = int(height*0.75), int(width*0.3)
        his_consumo_valor = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height*0.41), int(0)
        end_row, end_col = int(height*0.44), int(width*0.45)
        cliente_cpf = formatted_image[start_row:end_row, start_col:end_col]

    else:
        start_row, start_col = int(height*0.14), int(0)
        end_row, end_col = int(height*0.215), int(width*0.45)
        nome_endereco = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height*0.24), int(0)
        end_row, end_col = int(height*0.27), int(width*0.2)
        classe_instalacao = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height*0.16), int(width*0.7)
        end_row, end_col = int(height*0.2), int(width*1)
        num_cliente = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height*0.235), int(width*0.75)
        end_row, end_col = int(height*0.27), int(width*1)
        num_instalacao = formatted_image[start_row:end_row, start_col:end_col]

        start_row, start_col = int(height * 0.685), int(width*0.37)
    end_row, end_col = int(height*0.87), int(width*0.42)
    his_consumo_dias = formatted_image[start_row:end_row, start_col:end_col]
    
    start_row, start_col = int(height * 0.685), int(width*0.3)
    end_row, end_col = int(height*0.87), int(width*0.385)
    his_consumo_valor = formatted_image[start_row:end_row, start_col:end_col]

    # If you don't have tesseract executable in your PATH, include the following:
    tess.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'
    # Example tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'
    
    part1 = tess.image_to_string(nome_endereco, lang='por')
    part2 = tess.image_to_string(classe_instalacao, lang='por')
    part3 = tess.image_to_string(num_cliente, lang='por')
    part4 = tess.image_to_string(num_instalacao, lang='por')
    part5 = tess.image_to_string(his_consumo_dias, lang='por')
    
    if(height*1.0/width > 2):
        part6 = tess.image_to_string(cliente_cpf, lang='por')

    part7 = tess.image_to_string(his_consumo_valor, lang='por')
    
    
    part1 = re.sub(r'(\d)\s+(\d)', r'\1\2', part1)
    part1 = re.sub(r'(\d)\W+(\d)', r'\1\2', part1)
    part1 = ' '.join(part1.split())
    
    try:
        nome = re.search(r'(.+)?(?= AV)|(.+)?(?= RUA)', part1).group()
    except:
        nome = ""
    try:
        if(height*1.0/width > 2):
            endereco = re.search(r'( (AV(.+))| (RUA(.+)))', part1).group()
        else:
            endereco = re.search(r'( (AV(.+))| (RUA(.+)))(.+?(?=CPF))', part1).group()
    except:
        endereco = ""
    try:
        if(height*1.0/width > 2):
            cpf = ""
        else:
            cpf = re.search(r'\bCPF.(\d+)', part1).group(1)
    except:
        cpf = ""
    
    part2 = part2.replace(" ", "")
    part2 = ' '.join(part2.split())
    part2 = part2.replace("|", "")

    try:
        instalacao = part2
    except:
        instalacao = ""
    
    part3 = part3.replace(" ", "")
    part3 = part3.replace("|", "")
    
    try:
        numero_cliente = re.search(r'(\d+){6,}', part3).group()
    except:
        numero_cliente = ""
    
    part4 = part4.replace(" ", "")
    part4 = part4.replace("|", "")
    
    try:
        numero_inst = re.search(r'(\d+){6,}', part4).group()
    except:
        numero_inst = ""

    try:
        diasMes = re.search(r'([0-9\W]+)', part5).group()
    except:
        diasMes = ""
    
    if(height*1.0/width > 2):
        part6 = re.sub(r'(\d)\s+(\d)', r'\1\2', part6)
        part6 = re.sub(r'(\d)\W+(\d)', r'\1\2', part6)
        part6 = part6.replace(" ", "")
        part6 = part6.replace("|", "")
        try:
            cpf = re.search(r'\bCPF.(\d+)', part6).group(1)
        except:
            cpf = ""

    try:
        consumoDiario = re.search(r'([0-9\W]+)', part7).group()
    except:
        consumoDiario = ""
    
    print(nome.encode("utf-8"))
    print(endereco.encode("utf-8"))
    print(cpf.encode("utf-8"))
    print(instalacao.encode("utf-8"))
    print(numero_cliente.encode("utf-8"))
    print(numero_inst.encode("utf-8"))
    print(diasMes.encode("utf-8"))
    print(consumoDiario.encode("utf-8"))

    # cv.imshow("nome_endereco", nome_endereco)
    # cv.imshow("classe_instalacao", classe_instalacao)
    # cv.imshow("num_cliente", num_cliente)
    # cv.imshow("num_instalacao", num_instalacao)
    # cv.imshow("his_consumo_dias", his_consumo_dias)
    # cv.imshow("his_consumo_valor", his_consumo_valor)
    # cv.waitKey(0)
    # cv.destroyAllWindows()

    captured_info = {
            "nome": nome,
            "cpf": cpf,
            "endereco": endereco,
            "classeInstalacao": instalacao,
            "numeroInstalacao": numero_inst,
            "numeroCliente": numero_cliente,
            "quantidadeDias": diasMes,
            "consumoDiario": consumoDiario
    }
    
    return captured_info
    